create table  user
(
 id int primary key auto_increment,
 name varchar(200),
 password varchar(100),
 email varchar(30),
 date_joined datetime,
 preference varchar(200) default "all"

);

create table language_category_sub_category
(
 	id int primary key auto_increment,
	language varchar(200),
	category varchar(200),
	sub_category varchar(200),
	UNIQUE(language, category, sub_category)

);

create table  user_temporary
(
 id int primary key auto_increment,
 name varchar(200),
 password varchar(100),
 email varchar(30),
 date_joined datetime,
 confirm varchar(10)

);

create table video
(
 id int primary key AUTO_INCREMENT,
 video_key varchar(200),
 title varchar(400),
 language varchar(200),
 category varchar(200),
 sub_category VARCHAR(200),
 UNIQUE(video_key, category, sub_category)
);

/*
	category = "_some_category_name"
	sub_category = "_some_sub_category_name"
	create table if not exists category+sub_category
	(
		id int primary key auto_increment,
		video_id int,
		video_key varchar(200),
		title varchar(400),
		FOREIGN KEY(video_id) REFERNCES video(id)
	);

	language = "language video"
	create table language
	(
		id int primary key auto_increment,
		video_id int,
		video_key varchar(200),
		title varchar(400),
		FOREIGN KEY(video_id) REFERENCES video(id)
	);


*/

create table history
(
 id int primary key AUTO_INCREMENT,
 user_id int,
 video_id int,
 video_key varchar(200),
 title varchar(400),
 date_and_time datetime
);

create table activity
(
 id int primary key auto_increment,
 user_id int,
 video_id int,
 date_and_time datetime
);

create table friend
(
 id int primary key auto_increment,
 follower_id int,
 following_id int,
 date_followed datetime,
 FOREIGN KEY(follower_id) REFERENCES user(id),
 FOREIGN KEY(following_id) REFERENCES user(id),
 UNIQUE(id, follower_id, following_id)
);
