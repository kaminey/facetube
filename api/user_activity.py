from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from utils import application_constants
import MySQLdb

user_activity = Blueprint("user_activity", __name__)
@user_activity.route("/user_activity", methods = ["GET","POST"])
def main():
	if(request.method == "POST"):
		db = MySQLdb.connect(application_constants.SERVER, application_utils.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		video_key = request.form["video_key"]
		video_key = video_key.split("/")
		video_key = video_key[len(video_key)-1]
		user_id = session["user_id"]
		cursor.execute(""" select id from video where video_key = "%s" """%(video_key))
		video_id = cursor.fetchall()[0][0]
		date_time = datetime.datetime.now()
		f = "%Y-%m-%d %H:%M:%S"
		date_time = date_time.strftime(f)
		cursor.execute(""" insert into activity(user_id, video_id, date_and_time) values(%d,%d,%s) """%(user_id, video_id,date_time))
		cursor.execute("commit")
		return "application"
