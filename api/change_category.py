from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from flask import jsonify
from utils import application_constant
import MySQLdb

change_category = Blueprint("change_category", __name__)
@change_category.route("/change_category", methods = ["GET", "POST"])
def main():
	db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
	cursor = db.cursor()
	selected_category = request.form["selected_category"]
	session["category"] = selected_category
	session["sub_category"] = ""
	print selected_category
	cursor.execute("""select distinct sub_category from language_category_sub_category where language = "%s" and category = "%s" """%(session["language"],session["category"]))
	sub_categories = cursor.fetchall()
	return_response = {}
	return_response["sub_category"]=[]
	for sub_category in sub_categories:
		return_response["sub_category"].append(sub_category)
	return jsonify(return_response)
