from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from utils import application_constant
import MySQLdb


update_preference = Blueprint("update_preference", __name__)
@update_preference.route("/update_preference", methods = ["GET", "POST"])
def main():
	if(request.method == "POST"):
		print request.form["category"]
		id=session["user_id"]
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		cursor.execute("""update user set preference="%s" where id = %d """%(request.form["category"],id))
		cursor.execute("commit")
		return "application"

