from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from flask import jsonify
from utils import application_constant
import MySQLdb

get_subcategory_set_category = Blueprint("get_subcategory_set_category", __name__)
@get_subcategory_set_category.route("/get_subcategory_set_category/<category>", methods = ["GET", "POST"])
def main(category):
	print category
	db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
	cursor = db.cursor()
	session["category"] = category
	session["sub_category"]= "all"
	cursor.execute(""" select distinct sub_category from video where category = "%s" """%category)
	result = cursor.fetchall()
	sub_category = {}
	for ji in result:
		sub_category[ji[0]] = ji[0] 
	print sub_category
	return jsonify(sub_category)
	
