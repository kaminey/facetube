from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from utils import application_constant
import datetime
import MySQLdb

save_to_history = Blueprint("save_to_history", __name__)
@save_to_history.route("/save_to_history", methods = ["GET", "POST"])
def main():
	if(request.method == "POST"):
		user_id = session["user_id"]
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		video_key = request.form["video_key"]
		print video_key
		cursor.execute(""" select id, title from video where video_key = "%s" """%(video_key))
		result = cursor.fetchall()
		video_id = result[0][0]
		title = result[0][1]
		date_time = datetime.datetime.now()
		f = "%Y-%m-%d %H:%M:%S"
		date_time = date_time.strftime(f)
		cursor.execute(""" insert into history (user_id,video_id, video_key, title, date_and_time) values (%d,%d,"%s", "%s",  "%s") """%(user_id,video_id, video_key, title, date_time))
		cursor.execute("commit")
		return "application"
