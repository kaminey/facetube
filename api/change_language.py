from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from flask import jsonify
from utils import application_constant
import MySQLdb

change_language = Blueprint("change_language", __name__)
@change_language.route("/change_language", methods = ["GET", "POST"])
def main():
	db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
	cursor = db.cursor()
	selected_language = request.form["selected_language"]
	session["language"] = selected_language
	session["category"] = ""
	session["sub_category"] = ""
	print selected_language
	cursor.execute("""select distinct category from language_category_sub_category where language = "%s" """%(session["language"]))
	categories = cursor.fetchall()
	return_response = {}
	return_response["category"]=[]
	for category in categories:
		return_response["category"].append(category)
	return jsonify(return_response)
