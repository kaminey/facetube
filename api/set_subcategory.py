from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from flask import jsonify
from utils import application_constant
import MySQLdb

set_subcategory = Blueprint("set_subcategory", __name__)
@set_subcategory.route("/set_subcategory/<sub_category>", methods=["GET","POST"])
def main(sub_category):
	session["sub_category"] = sub_category
	return "{}"
