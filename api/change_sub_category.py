from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from flask import jsonify
from utils import application_constant
import MySQLdb

change_sub_category = Blueprint("change_sub_category", __name__)
@change_sub_category.route("/change_sub_category", methods = ["GET", "POST"])
def main():
	selected_sub_category = request.form["selected_sub_category"]
	print selected_sub_category
	session["sub_category"] = selected_sub_category
	return "1"
