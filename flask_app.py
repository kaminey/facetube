from flask import Flask
from views.login import login
#from views.signup import signup
from views.verification_link_generation import verification_link_generation
from views.verify_account import verify_account
from views.logout import logout
from views.home import home
from api.save_to_history import save_to_history
from views.show_history import show_history
from views.upload import upload
from api.change_language import change_language
from api.play_video import play_video
from api.change_category import change_category
from api.change_sub_category import change_sub_category
from api.set_subcategory import set_subcategory
from api.get_subcategory_set_category import get_subcategory_set_category
from api.update_preference import update_preference
import os


base_addr = "http://127.0.0.1:5000/"
app = Flask(__name__)

app.config['UPLOAD FOLDER'] = 'static/'
app.config['ALLOWED EXTENSIONS'] = set(['jpg','jpeg','gif','png'])

app.register_blueprint(login)
app.register_blueprint(verification_link_generation)
app.register_blueprint(verify_account)

#app.register_blueprint(signup)
app.register_blueprint(logout)
app.register_blueprint(upload)
app.register_blueprint(home)
app.register_blueprint(save_to_history)
app.register_blueprint(show_history)
app.register_blueprint(change_language)
app.register_blueprint(play_video)
app.register_blueprint(change_sub_category)
app.register_blueprint(change_category)
app.register_blueprint(set_subcategory)
app.register_blueprint(get_subcategory_set_category)
app.register_blueprint(update_preference)

app.secret_key = os.urandom(24)
if __name__ == "__main__":
	app.run(debug = True)

