from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from werkzeug.security import check_password_hash
from flask import url_for
from utils import application_constant
import MySQLdb

login = Blueprint("login", __name__)
@login.route("/login", methods = ["GET", "POST"])
def main():
	if(request.method == "GET"):
		if( session["email"]):
			return redirect(url_for("home.main"))
		else:
			return redirect(url_for("home.main"))

	else:
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		email = request.form["email"]
		password = request.form["password"]
		cursor.execute("""select password from user where email="%s"  """%(email))
		result = cursor.fetchall()
		if(result and check_password_hash(result[0][0], password)):
			session["email"] = email
			cursor.execute("""select id from user where email = "%s" """%(email))
			result=cursor.fetchall()
			session["user_id"] = result[0][0]
			print session["user_id"]
			return redirect(url_for("home.main"))
		else:
			return redirect(url_for("home.main"))
