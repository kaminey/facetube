from flask import Blueprint
from flask import render_template
from flask import session
from utils import application_constant
import random
import MySQLdb

def get_dashed_string(a):
	if a:
		b = a.split()
		b = "_".join(b)
		return "_"+b
	else:
		return ""

def table_name_create(language, category, sub_category):
	tb_language = get_dashed_string(language)
	tb_category = get_dashed_string(category)
	tb_sub_category = get_dashed_string(sub_category)
	table_name_tb = tb_language[1:len(tb_language)]+  tb_category +  tb_sub_category
	print table_name_tb
	return table_name_tb


home = Blueprint("home", __name__)
@home.route("/")
def main():
	db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
	cursor = db.cursor()
	try:
		session["language"]
	except:
		session["language"] = "Hindi"
		session["category"] = ""
		session["sub_category"] = ""
		session["email"] = ""
	cursor.execute(""" select max(id) from %s """%table_name_create(session["language"],session["category"],session["sub_category"]))
	id = cursor.fetchone()[0]
	random_id = random.randint(0,id+1)
	print random_id,"*******************************************************"
	cursor.execute(""" select video_key from %s where id = %d"""%(table_name_create(session["language"],session["category"],session["sub_category"]), random_id))
	video_id = cursor.fetchone()[0]
	print video_id,"********************************************************"
	t = render_template("home3.html",base_addr = application_constant.BASE_ADDR, key = video_id, language_options = ["English","Hindi"], user_email = session["email"])
	#print t
	return t
