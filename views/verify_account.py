from flask import Blueprint
from flask import render_template
import itsdangerous
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import redirect
from flask import url_for
from utils import application_constant
import MySQLdb

verify_account = Blueprint("verify_account", __name__)

@verify_account.route("/verify_account/<token>")
def main(token):

	db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
	cursor = db.cursor()
	s = Serializer("abc", salt = "activate-salt")
	try:
		data = s.loads(token)
		id = data["id"]
		cursor.execute(" select confirm from user_temporary where id = %d "%id)
		confirmed = cursor.fetchall()[0][0]
		if(confirmed == "false"):
			cursor.execute(""" update user_temporary set confirm="true" where id = %d"""%id)
			cursor.execute(""" select name, password, email, date_joined from user_temporary where id = %d"""%id)
			result = cursor.fetchall()[0]
			name = result[0]
			password = result[1]
			email = result[2]
			date_joined = result[3]
			cursor.execute(""" insert into user (name, password, email, date_joined) values("%s", "%s", "%s", "%s") """%(name, password, email, date_joined))
			cursor.execute("commit")
			message ="your account has been verified"
			return render_template("verify_account.html", message = message)
		else:
			return redirect(url_for("login.main"))
	except itsdangerous.SignatureExpired:
	    message = "Your verification period has expired please issue a new verification link"
	    return render_template("verify_account.html", message = message)
	cursor.close()
	db.close()







