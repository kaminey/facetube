from flask import Blueprint
from flask import render_template
from utils import application_constant
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from werkzeug import secure_filename
import os
import MySQLdb

upload = Blueprint("upload",__name__)

def allowed(filename):
	return '.' in filename and filename.rsplit('.',1)[1] in application_constant.ALLOWED_EXTENSIONS

@upload.route("/upload",methods=['POST'])
def main():
	if(session.get("user_id",False)):
		file = request.files['file']
		if file and allowed(file.filename):
			filename = secure_filename(file.filename)
			file.save(os.path.join(application_constant.UPLOAD_FOLDER,filename))
			db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
			cursor = db.cursor()
			cursor.execute(""" insert into user(profile_pic) values ("%s") """%filename)

			return redirect(url_for("show_history.main"))



	else:
	    return redirect(url_for("home.main"))


