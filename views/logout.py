from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
import MySQLdb



logout = Blueprint("logout", __name__)
@logout.route("/logout")
def main():
	session.pop("email",None)
	session.pop("user_id",None)
	session.pop("language",None)
	session.pop("category",None)
	session.pop("sub_category",None)
	return redirect(url_for("home.main"))
