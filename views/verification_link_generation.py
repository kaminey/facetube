from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from werkzeug.security import generate_password_hash
from flask import redirect
from flask import url_for
from send_email import send_email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from utils import application_constant
import datetime
import MySQLdb

verification_link_generation = Blueprint("verification_link_generation", __name__)
@verification_link_generation.route("/signup", methods= ["GET", "POST"])
def main():
	if(request.method == "GET"):
		if(session["email"]):
			return redirect(url_for("home.main"))
		else:
			return render_template("signup.html")
	else:
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		name = request.form["name"]
		email = request.form["email"]
		password = request.form["password"]
		password = generate_password_hash(password)
		cursor.execute("""select email from user_temporary where email = "%s" """%(email))
		result=cursor.fetchall()
		if(not result):
			date_time = datetime.datetime.now()
			f = "%Y-%m-%d %H:%M:%S"
			date_time = date_time.strftime(f)
			confirmed = "false"
			cursor.execute("""insert into user_temporary(name,email,password, date_joined, confirm) values ("%s","%s","%s","%s","%s") """%(name,email,password, date_time, confirmed))
			cursor.execute("commit")
			cursor.execute("""select id from user_temporary where email = "%s" """%(email))
			id = cursor.fetchall()[0][0]
			s = Serializer("abc", salt = "activate-salt", expires_in = 3600)
			token = s.dumps({ 'id': id })
			cursor.close()
			db.close()
			msg = MIMEMultipart('alternative')
			t = render_template("verification_link_generation.html", name = name, token = token)
			to_be_sent = MIMEText(t,"html")
			msg.attach(to_be_sent)
			send_email(msg.as_string(),email)
			return "please check your email"
		else:
			cursor.execute("""select id,confirm from user_temporary where email = "%s"  """%(email))
			result = cursor.fetchall()[0]
			id = result[0]
			confirmed = result[1]
			if(confirmed == "false"):
				s = Serializer("abc", salt = "activate-salt", expires_in = 50)
				token = s.dumps({ 'id': id })
				return token+"this is seconed time"
			else:
				return "this account already exists"

