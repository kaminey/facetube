from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from flask import redirect
from flask import url_for
from utils import application_constant
import MySQLdb

show_history = Blueprint("show_history", __name__)
@show_history.route("/show_history")
def main():
	if(session.get("user_id",False)):
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		cursor.execute("select video_key,date_and_time, title from history where user_id = %d order by date_and_time"%session["user_id"])
		result = cursor.fetchall()
		cursor.execute("select name from user where id = %d"%session["user_id"])
		result2 = cursor.fetchall()
		cursor.execute("select profile_pic from user where id = %d"%session["user_id"])
		result3 = cursor.fetchall()
		video_keys = []
		user_info = result2[0][0]
		profile_pic = result3[0][0]
		for row in result:
			video_keys.append(row)
		video_keys.reverse()
		return render_template("history.html", video_keys = video_keys,user_info = user_info,profile_pic = profile_pic, user_email = session["email"])
	else:
	    return redirect(url_for("home.main"))


