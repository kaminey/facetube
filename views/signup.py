from flask import Blueprint
from flask import Flask
from flask import render_template
from flask import session
from flask import request
from werkzeug.security import generate_password_hash
from flask import redirect
from flask import url_for
from utils import application_constant
import datetime
import MySQLdb

signup = Blueprint("signup", __name__)
@signup.route("/signup", methods= ["GET", "POST"])
def main():
	if(request.method == "GET"):
		if(session["email"]):
			return redirect(url_for("home.main"))
		else:
			return render_template("signup.html")
	else:
		db = MySQLdb.connect(application_constant.SERVER, application_constant.USER, application_constant.PASSWORD, application_constant.DATABASE)
		cursor = db.cursor()
		name = request.form["name"]
		email = request.form["email"]
		password = request.form["password"]
		password = generate_password_hash(password)
		cursor.execute("""select email from user where email = "%s" """%(email))
		result=cursor.fetchall()
		if(not result):
			date_time = datetime.datetime.now()
			f = "%Y-%m-%d %H:%M:%S"
			date_time = date_time.strftime(f)
			cursor.execute("""insert into user(name,email,password, date_joined) values ("%s","%s","%s","%s") """%(name,email,password, date_time))
			cursor.execute("commit")
			cursor.close()
			db.close()
			return redirect(url_for("login.main"))
		else:
			cursor.close()
			db.close()
			return render_template("signup.html", error="This account already exists")

